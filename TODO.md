DEBIAN TODO
Maytham Alsudany <maytha8thedev@gmail.com>

# Packaging

 - tarzeau's d/copyrights
 [5:03:47 pm] <tarzeau_> do you feel like helping with d/copyright on my ITPs?
 [5:04:04 pm] <maytham> sure
 [5:05:11 pm] <tarzeau_> i have http://bananas.ethz.ch/debian/dwarfs/ with decopy.txt there and the 0.9.9-1.dsc
 [5:05:37 pm] <tarzeau_> also have furnace http://sid.ethz.ch/debian/furnace/2024d/
   
   - glaximate

 - Update kitty
   - Pending upload

 <!-- - Reply to quickemu RFS -->
 <!--     Just because it _can_ download non-free stuff does not mean it is contrib -->
 <!--     (contrib is for _needs_ non-free or downloads). Prime example is debgpt, -->
 <!--     which is in main but "downloads" from OpenAI. -->

 - Package gtklock-powerbar-module
 - Upload gtklock-userinfo-module and gtklock-playerctl-module

 - Update cjson, adopt, and upload

## Redict team

 - Get upload rights for libhdr-histogram
 - Add autopkgtest d/tests to:
   - libhdr-histogram
 - Source-only upload of libhdr-histogram
 - Make redict deb use libhdr-histogram
 - Make redict deb use system lua5.1 (upload to experimental)

## Python team

:)

## Go team

 - Update golang-gvisor-gvisor (PAIN)
   - Then ping Eugen Stan (ieugen) <eugen دوت stan آت netdava دوت com>
 - Package galene
   - Package golang deps
   - Devendor and package contextual.js and toastify.js
 - Package vale
 - Package websocat

## Rust team

 - Upload unreleased rust-numbat deps..**progressing**
   - only numbat & numbat-cli remaining
 - Upload prr and deps
 - Package (rust-)hurl and deps -- Upstream is getting desperate...
 - Package avis-img and friends https://salsa.debian.org/rust-team/debcargo-conf/-/merge_requests/602
   - Package eframe+egui and friends https://salsa.debian.org/rust-team/debcargo-conf/-/merge_requests/640
 - Package anyrun outside of Rust team
     (not published to crates.io, migration not happening any time soon) https://bugs.debian.org/1057118

# Non-packaging

 - Finish DDTP Salsa login support MR https://salsa.debian.org/l10n-team/ddtp/-/merge_requests/48
 - debian-policy proposal needs a nudge -- nudged 20/06/2024
 - Debian website overhaul
 - Work on gophian
 - tarzeau: port bubnbros to python3
   [5:07:10 pm] <tarzeau_> https://sourceforge.net/projects/bubnbros/ this was
                           in debian as bubbros but fell out, me and someone
                           else tried with python2to3 upgrade and failed. i got
                           input try transpiler make a c port, as pygame also
                           exists with sdl bindings
   [5:07:54 pm] <tarzeau_> if link is broken, i have copy at: https://github.com/alexmyczko/bubbros

# Wishlist

 - Work on translating package descs thru DDTP
 - Translate apt https://salsa.debian.org/apt-team/apt/-/blob/main/po/apt-all.pot?ref_type=heads
   - Retranslate the whole thing under a new license?
 - Translate debian-edu-doc https://salsa.debian.org/debian-edu/debian-edu-doc https://hosted.weblate.org/projects/debian-edu-documentation/
 - Translate bits


## Packaging Wishlist

 - skiane
 - valent
 - opensnitch (RFH)
